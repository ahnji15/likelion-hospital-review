package com.example.likelionhospitalreview.domain.dto;

import com.example.likelionhospitalreview.domain.entity.User;
import lombok.Getter;

@Getter
public class UserJoinRequest {
    private String userName;
    private String password;

    public User toEntity(String encodedPassword) {
        return User.builder()
                .userName(this.userName)
                .password(encodedPassword)
                .build();
    }
}
