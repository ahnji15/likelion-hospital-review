package com.example.likelionhospitalreview.service;

import com.example.likelionhospitalreview.domain.dto.UserJoinRequest;
import com.example.likelionhospitalreview.domain.entity.User;
import com.example.likelionhospitalreview.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;

    public User join(UserJoinRequest request) {
        // userName 중복 체크
        userRepository.findByUserName(request.getUserName())
                .ifPresent(user -> {
                    throw new RuntimeException("Username이 중복됩니다.");
                });

        // password encoding해서 저장
        return userRepository.save(request.toEntity(encoder.encode(request.getPassword())));
    }
}
